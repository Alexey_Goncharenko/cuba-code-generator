// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

import store from './store';

import AppComponent from './components/app';

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <AppComponent/>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
