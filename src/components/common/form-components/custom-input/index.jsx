import React from 'react';
import PropTypes from 'prop-types';
import {ControlLabel, FormControl, FormGroup, HelpBlock, InputGroup} from 'react-bootstrap';

class CustomInput extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    withAddon: PropTypes.bool,
    label: PropTypes.any
  };

  static defaultProps = {
    className: ''
  };

  render() {
    const {withAddon, label, input, meta: {error, touched}, disabled, ...props} = this.props;

    const inputRenderers = [
      withAddon && (
        <InputGroup.Addon>
          <i className='fa fa-usd'/>
        </InputGroup.Addon>
      ), (
        <FormControl
          disabled={disabled}
          type='text'
          {...input}
          {...props}
        />
      ), error && touched && (<HelpBlock>{error}</HelpBlock>)
    ];

    return (
      <FormGroup controlId={input.name} validationState={error && touched ? 'error' : null}>
        {label && (<ControlLabel>{label}</ControlLabel>)}
        {withAddon && (
          <InputGroup>
            {inputRenderers[0]}
            {inputRenderers[1]}
          </InputGroup>
        )}
        {!withAddon && (inputRenderers[0])}
        {!withAddon && (inputRenderers[1])}
        {inputRenderers[2]}
      </FormGroup>
    );
  }
}

export default CustomInput;
