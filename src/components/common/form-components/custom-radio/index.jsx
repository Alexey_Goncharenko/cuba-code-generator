import React from 'react';
import PropTypes from 'prop-types';
import './radio-button.scss';

class Radio extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    onChange: PropTypes.func,
    label: PropTypes.any
  };

  static defaultProps = {
    className: ''
  };

  render() {
    const {className, label, input, meta, onChange, ...props} = this.props;

    return (
      <label className={`radio-button ${className}`}>
        <input
          className='radio-button__input'
          type='radio'
          onChange={e => {
            if (onChange) {
              onChange(e);
            }
            if (input && input.onChange) {
              input.onChange(e);
            }
          }}
          {...input}
          {...props}
        />
        <div className='radio-button__label'>{label}</div>
      </label>
    );
  }
}

export default Radio;
