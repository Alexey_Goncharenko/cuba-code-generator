import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

class CustomSelect extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    onChange: PropTypes.func,
    label: PropTypes.any
  };

  static defaultProps = {
    className: ''
  };

  render() {
    const {className, label, input, meta, onChange, ...props} = this.props;

    return (
      <Select
        {...input}
        {...props}
        value={input.value}
        onChange={input.onChange}
        onBlur={() => input.onBlur(input.value)}
      />
    );
  }
}

export default CustomSelect;
