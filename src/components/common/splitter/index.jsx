import React from 'react';
import PropTypes from 'prop-types';

import './splitter.scss';

class Splitter extends React.Component {
  static propTypes = {
    left: PropTypes.any,
    right: PropTypes.any,
    header: PropTypes.string
  };

  render() {
    const {left, right, header} = this.props;

    return (
      <div className='splitter'>
        <h3 className='splitter__header'>{header}</h3>
        <div className='splitter__left'>{left}</div>
        <div className='splitter__right'>{right}</div>
      </div>
    );
  }
}

export default Splitter;
