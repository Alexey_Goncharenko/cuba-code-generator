import React from 'react';
import PropTypes from 'prop-types';
import {Glyphicon} from 'react-bootstrap';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {ToastContainer, toast} from 'react-toastify';
import AceEditor from 'react-ace';

import 'brace/mode/java';
import 'brace/mode/xml';
import 'brace/theme/monokai';
import 'react-toastify/dist/ReactToastify.min.css';

class CodeEditor extends React.Component {
  static propTypes = {
    filename: PropTypes.string,
    code: PropTypes.string,
    mode: PropTypes.string,
    height: PropTypes.string
  };

  static defaultProps = {
    height: '500px',
    mode: 'java'
  };

  notifyCopy = () => toast('Скопировано в буфер');

  render() {
    const {filename, code, height, mode} = this.props;

    return (
      <div className='result'>
        <ToastContainer autoClose={2000} />
        <div className='result__header'>
          <CopyToClipboard text={code} onCopy={this.notifyCopy}>
            <Glyphicon
              glyph='copy'
              bsClass='result__header__icon glyphicon'
              title='Скопировать в буфер'
            />
          </CopyToClipboard>
          <span className='result__header__filename'>{filename}</span>
        </div>
        <AceEditor
          mode={mode}
          theme='monokai'
          name='code'
          fontSize={14}
          showPrintMargin
          width='100%'
          height={height}
          showGutter
          highlightActiveLine
          value={code}
          setOptions={{
            enableSnippets: false,
            showLineNumbers: true,
            tabSize: 4,
            readOnly: true
          }}/>

      </div>
    );
  }
}

export default CodeEditor;
