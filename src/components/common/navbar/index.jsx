import React from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import {withRouter, Link} from 'react-router-dom';


class NavBarComponent extends React.Component {
  render() {
    return (
      <Navbar staticTop>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to='/'>CUBA Generator</Link>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1} href='/entity-generator'>
            Табличная сущность
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
}


export default withRouter(NavBarComponent);
