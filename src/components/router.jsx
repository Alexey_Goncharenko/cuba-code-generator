import React from 'react';
import {Route, withRouter, Redirect} from 'react-router-dom';

import NavBarComponent from './common/navbar';

import EntityGenerator from './entity-generator';

class RouterComponent extends React.Component {
  render() {
    return (
      <div>
        <NavBarComponent />

        <Route exact path='/entity-generator' component={EntityGenerator}/>

        <Route exact path="/" render={() => (<Redirect to='/entity-generator' />)} />
      </div>
    );
  }
}

export default withRouter(RouterComponent);
