import React from 'react';
import {BrowserRouter, withRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

import RouterComponent from './router';
import store from '../store';

class AppComponent extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <RouterComponent/>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default withRouter(AppComponent);
