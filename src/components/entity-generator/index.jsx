import React from 'react';
import PropTypes from 'prop-types';
import {reduxForm, formValueSelector} from 'redux-form';
import {connect} from 'react-redux';
import {PageHeader} from 'react-bootstrap';

import ProcedureGenerator from './components/procedure';

class EntityGenerator extends React.Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired,
    change: PropTypes.func.isRequired,
    inputParameters: PropTypes.array,
    procedureName: PropTypes.string,
    columns: PropTypes.array
  };

  static defaultProps = {
    inputParameters: [],
    columns: []
  };

  static formName = 'EntityGeneratorForm';

  render() {
    const {procedureName, columns, initialize, inputParameters, change} = this.props;

    return (
      <div>
        <div className='content-wrapper'>
          <PageHeader>
            Генератор табличной сущности
          </PageHeader>

          <ProcedureGenerator
            change={change}
            columns={columns}
            name={procedureName}
            initialize={initialize}
            inputParameters={inputParameters}
          />
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const selector = formValueSelector(EntityGenerator.formName);

  const filterColumns = (columns = []) => columns.filter(i => i.name);
  const filterParameters = (parameters = []) => parameters.filter(i => i);

  return {
    procedureName: selector(state, 'procedureName'),
    columns: filterColumns(selector(state, 'columns')),
    inputParameters: filterParameters(selector(state, 'inputParameters'))
  };
};

const form = reduxForm({
  form: EntityGenerator.formName
})(EntityGenerator);

export default connect(mapStateToProps)(form);
