import React from 'react';
import PropTypes from 'prop-types';
import {Field} from 'redux-form';
import debounce from 'lodash/debounce';

import {getFileName, getPackageName, generateEntityCode, generateServiceCode, generateServiceBeanCode,
  generateWebSpringCode, generateDatasourceCode, generateGenericUICode} from 'helpers';

import Splitter from 'components/common/splitter';
import CodeEditor from 'components/common/code-editor';
import CustomInput from 'components/common/form-components/custom-input';

import Columns from './columns';

import './procedure.scss';
import InputParameters from './input-parameters';

class ProcedureGenerator extends React.Component {
  static propTypes = {
    change: PropTypes.func.isRequired,
    initialize: PropTypes.func.isRequired,
    name: PropTypes.string,
    columns: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      type: PropTypes.oneOf(['String', 'Integer', 'Double'])
    })),
    inputParameters: PropTypes.arrayOf(PropTypes.string)
  };

  static fieldsMap = {
    NUMBER: 'Integer',
    STRING: 'String',
    FIXED_CHAR: 'String'
  };

  state = {
    columnsCount: 0,
    parametersCount: 0
  };

  handleChangeName = debounce(e => {
    const procedureName = e.target.value;

    return fetch(`https://api.ciu.nstu.ru/v1.0/test/func_descr?func=${procedureName}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'http-api-key': 'Y!@#13dft456DGWEv34g435f'
      }
    }).then(res => res.json())
      .then(({fields, params, error}) => {
        if (error) {
          return;
        }

        this.setState({columnsCount: fields.length, parametersCount: params.filter(i => i.ARGUMENT_NAME).length});

        return this.props.initialize({
          procedureName,
          inputParameters: params.filter(i => i.ARGUMENT_NAME).map(i => i.ARGUMENT_NAME),
          columns: fields.map(([name, type]) => ({name, type: ProcedureGenerator.fieldsMap[type]}))
        });
      });
  }, 300);

  render() {
    const {name, columns, inputParameters, change} = this.props;

    const fileName = getFileName(name);
    const packageName = getPackageName(name);

    const entityFilePath = name && `/modules/global/src/com/company/rgr/entity/${packageName}/${fileName}.java`;
    const serviceFilePath = name && `/modules/global/src/com/company/rgr/service/${packageName}/${fileName}Service.java`;
    const serviceBeanFilePath = name && `/modules/core/src/com/company/rgr/service/${packageName}/${fileName}ServiceBean.java`;
    const webSpringFilePath = '/modules/web/src/com/company/rgr/web-spring.xml';
    const datasourceFilePath = name && `/modules/web/src/com/company/rgr/web/customdatasources/${fileName}Datasource.java`;
    const genericUIFilePath = name && '/modules/web/src/com/company/rgr/web/<YourXMLFileName>.xml';

    return (
      <Splitter
        header={'Процедура'}
        left={(
          <div>
            <div className='col-sm-12'>
              <Field
                type='text'
                name='procedureName'
                label='Полное название хранимой процедуры'
                placeholder='Например, decanatuser.tr_work_pkg.get_src_lines'
                component={CustomInput}
                onChange={this.handleChangeName}
              />
            </div>

            <InputParameters
              change={change}
              inputParameters={inputParameters}
              parametersCount={this.state.parametersCount}
              onChange={(parametersCount, cb) => this.setState({parametersCount}, cb)}
            />

            <Columns
              change={change}
              columns={columns}
              columnsCount={this.state.columnsCount}
              onChange={(columnsCount, cb) => this.setState({columnsCount}, cb)}
            />

          </div>
        )}
        right={(
          <div>
            <CodeEditor filename={entityFilePath} code={generateEntityCode(name, columns)}/>
            <CodeEditor height='250px' filename={serviceFilePath} code={generateServiceCode(name, inputParameters)}/>
            <CodeEditor height='250px' filename={serviceBeanFilePath} code={generateServiceBeanCode(name, inputParameters)}/>
            <CodeEditor height='250px' filename={webSpringFilePath} code={generateWebSpringCode(name)} mode='xml'/>
            <CodeEditor filename={datasourceFilePath} code={generateDatasourceCode(name, inputParameters)}/>
            <CodeEditor filename={genericUIFilePath} code={generateGenericUICode(name, columns)} mode='xml'/>
          </div>
        )}
      />
    );
  }
}

export default ProcedureGenerator;
