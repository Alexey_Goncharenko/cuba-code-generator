import React from 'react';
import PropTypes from 'prop-types';
import {ControlLabel, Glyphicon} from 'react-bootstrap';
import {Field, FormSection} from 'redux-form';

import CustomInput from 'components/common/form-components/custom-input';
import CustomSelect from 'components/common/form-components/custom-select';

class Columns extends React.Component {
  static propTypes = {
    columns: PropTypes.array,
    change: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    columnsCount: PropTypes.number.isRequired
  };

  handleRemoveColumn = id => () => {
    const {columns, change, columnsCount, onChange} = this.props;
    for (let i = id; i < columnsCount - 1; i++) {
      change(`columns.${i}`, columns[i + 1]);
    }

    change(`columns.${columnsCount - 1}`, {});

    onChange(columnsCount - 1);
  };

  render() {
    const typeOptions = ['String', 'Integer', 'Double'].map(label => ({label, value: label}));

    const {columnsCount, onChange} = this.props;

    return (
      <div>
        <div className='col-sm-12'>
          <ControlLabel>Возвращаемые колонки</ControlLabel>
        </div>
        <FormSection name='columns'>
          {columnsCount > 0 && [...new Array(columnsCount)].map((s, id) => (
            <FormSection name={`${id}`} key={id}>
              <div className='col-sm-4'>
                <Field
                  type='text'
                  name='name'
                  placeholder='Название в БД'
                  component={CustomInput}
                />
              </div>
              <div className='col-sm-3'>
                <Field
                  type='select'
                  name='type'
                  placeholder='Тип'
                  component={CustomSelect}
                  simpleValue
                  clearable={false}
                  options={typeOptions}
                />
              </div>
              <div className='col-sm-4'>
                <Field
                  type='text'
                  name='title'
                  placeholder='Заголовок в таблице'
                  component={CustomInput}
                />
              </div>
              <div className='cursor-pointer icon-wrapper pull-left' onClick={this.handleRemoveColumn(id)}>
                <Glyphicon glyph='remove'/>
              </div>
            </FormSection>
          ))}
          <FormSection name={`${columnsCount}`}>
            <div className='col-sm-4'>
              <Field
                type='text'
                name='name'
                placeholder='Название в БД'
                component={CustomInput}
                className='float-input'
                onChange={() => onChange(columnsCount + 1, () => {
                  const input = document.getElementsByName(`columns.${columnsCount}.name`)[0];
                  if (input) {
                    input.focus();
                    input.selectionStart = input.value.length;
                  }
                  this.props.change(`columns.${columnsCount}.type`, 'String');
                })}
              />
            </div>
          </FormSection>
        </FormSection>
      </div>
    );
  }
}

export default Columns;
