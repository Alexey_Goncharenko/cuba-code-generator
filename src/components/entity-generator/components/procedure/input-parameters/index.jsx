import React from 'react';
import PropTypes from 'prop-types';
import {ControlLabel, Glyphicon} from 'react-bootstrap';
import {Field, FormSection} from 'redux-form';

import CustomInput from 'components/common/form-components/custom-input';

class InputParameters extends React.Component {
  static propTypes = {
    change: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    inputParameters: PropTypes.array.isRequired,
    parametersCount: PropTypes.number.isRequired
  };

  handleRemoveParameter = id => () => {
    const {inputParameters, change, parametersCount, onChange} = this.props;
    for (let i = id; i < parametersCount - 1; i++) {
      change(`inputParameters.${i}`, inputParameters[i + 1]);
    }

    change(`inputParameters.${parametersCount - 1}`, null);

    onChange(parametersCount - 1);
  };

  render() {
    const {parametersCount, onChange} = this.props;

    return (
      <div>
        <div className='col-sm-12'>
          <ControlLabel>Аргументы</ControlLabel>
        </div>
        <FormSection name='inputParameters'>
          {parametersCount > 0 && [...new Array(parametersCount)].map((s, id) => (
            <div key={id}>
              <div className='col-sm-11'>
                <Field
                  type='text'
                  name={`${id}`}
                  placeholder='Название аргумента'
                  component={CustomInput}
                />
              </div>
              <div className='cursor-pointer icon-wrapper pull-left' onClick={this.handleRemoveParameter(id)}>
                <Glyphicon glyph='remove'/>
              </div>
            </div>
          ))}
          <div className='col-sm-11'>
            <Field
              type='text'
              name={`${parametersCount}`}
              placeholder='Название аргумента'
              component={CustomInput}
              className='float-input'
              onChange={() => onChange(parametersCount + 1, () => {
                const input = document.getElementsByName(`inputParameters.${parametersCount}`)[0];
                if (input) {
                  input.focus();
                  input.selectionStart = input.value.length;
                }
              })}
            />
          </div>
        </FormSection>
      </div>
    );
  }
}

export default InputParameters;
