// import {createLogger} from 'redux-logger';
import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './ducks/main-reducer';

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware
  )
);

store.subscribe(() => {

});

export default store;
