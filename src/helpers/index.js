import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

export function getFileName(procedurePath) {
  if (!procedurePath) {
    return '';
  }

  const procedureName = procedurePath.split('.').slice(-1)[0];

  const clearName = procedureName.startsWith('get_') ? procedureName.slice(4) : procedureName;
  return upperFirst(camelCase(clearName));
}

export function getPackageName(procedurePath) {
  if (!procedurePath) {
    return '';
  }

  const packageName = procedurePath.split('.').slice(-2)[0];
  return upperFirst(camelCase(packageName));
}

export function generateEntityCode(procedurePath, columns = []) {
  const className = getFileName(procedurePath);
  const packageName = getPackageName(procedurePath);
  if (!className) {
    return '';
  }

  return `package com.company.rgr.entity.fromOracle.${packageName};

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.chile.core.annotations.MetaProperty;

import com.haulmont.cuba.core.entity.BaseLongIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;

@SuppressWarnings("JpaDataSourceORMInspection")
@NamePattern("%s|id")
@MetaClass(name = "rgr$${className}")
@Entity
public class ${className} extends BaseLongIdEntity {
${columns.map(column => `
    @MetaProperty
    @Column(name = "${column.name}")
    protected ${column.type} ${camelCase(column.name)};
`).join('')}
${columns.map(column => {
    const camelName = camelCase(column.name);
    const upperName = upperFirst(camelName);

    return `
    public ${column.type} get${upperName}() { return ${camelName}; }

    public void set${upperName}(${column.type} ${camelName}) { this.${camelName} = ${camelName}; }
`;
  }).join('')}
}`;
}

export function generateServiceCode(procedurePath, inputParameters = []) {
  const entityName = getFileName(procedurePath);
  const packageName = getPackageName(procedurePath);
  if (!entityName) {
    return '';
  }

  const params = inputParameters.map(i => camelCase(i));

  return `package com.company.rgr.service.${packageName};

import com.company.rgr.entity.fromOracle.${packageName}.${entityName};

import java.util.List;

public interface SrcLineService {
    String NAME = "rgr_${entityName}Service";

    List<${entityName}> get${entityName}(${params.map(i => `Object ${i}`).join(', ')});
}`;
}

export function generateServiceBeanCode(procedurePath, inputParameters = []) {
  const entityName = getFileName(procedurePath);
  const packageName = getPackageName(procedurePath);
  if (!entityName) {
    return '';
  }

  const params = inputParameters.map(i => camelCase(i));

  return `package com.company.rgr.service.${packageName};

import com.company.rgr.entity.fromOracle.${packageName}.${entityName};
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(${entityName}Service.NAME)
public class ${entityName}ServiceBean implements ${entityName}Service {

    @Inject
    private EntityService<${entityName}> entityService;

    public List<${entityName}> get${entityName}(${params.map(i => `Object ${i}`).join(', ')}) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        ${params.map((param, index) => `arguments.put(${index + 1}, ${param});
        `).join('')}

        return entityService.fetchData("${procedurePath}(${params.map(() => '?').join(', ')})", ${entityName}.class, arguments, true);
    }
}`;
}

export function generateDatasourceCode(procedurePath, inputParameters = []) {
  const entityName = getFileName(procedurePath);
  const packageName = getPackageName(procedurePath);
  if (!entityName) {
    return '';
  }

  const params = inputParameters.map(i => camelCase(i));

  return `package com.company.rgr.web.customdatasources;

import com.company.rgr.entity.fromOracle.${packageName}.${entityName};
import com.company.rgr.service.${packageName}.${entityName}Service;
import com.haulmont.cuba.core.global.AppBeans;

import java.util.Collection;
import java.util.Map;

import com.haulmont.cuba.gui.data.impl.CustomCollectionDatasource;

public class ${entityName}Datasource extends CustomCollectionDatasource<${entityName}, Long> {

    private ${entityName}Service ${camelCase(entityName)}Service = AppBeans.get(${entityName}Service.NAME);

    @Override
    protected Collection<${entityName}> getEntities(Map<String, Object> params) {
        return ${camelCase(entityName)}Service.get${entityName}(
                ${params.map(param => `params.getOrDefault("${param}", null)`).join(`,
                `)}
        );
    }
}`;
}

export function generateWebSpringCode(procedurePath) {
  const entityName = getFileName(procedurePath);
  const packageName = getPackageName(procedurePath);
  if (!entityName) {
    return '';
  }

  return `<beans ...>
    <bean ...>
        <property name="remoteServices">
            <map>
                ...
                <entry key="rgr_${entityName}Service"
                       value="com.company.rgr.service.${packageName}.${entityName}Service"/>
            </map>
        </property>
    </bean>
</beans>`;
}

export function generateGenericUICode(procedurePath, columns = []) {
  const entityName = getFileName(procedurePath);
  const packageName = getPackageName(procedurePath);
  if (!entityName) {
    return '';
  }

  const displayedColumns = columns
    .filter(i => i.title)
    .map(i => ({...i, name: camelCase(i.name)}));

  return `<window ...>
    <dsContext>
        ...
        <collectionDatasource id="${camelCase(entityName)}"
                              class="com.company.rgr.entity.fromOracle.${packageName}.${entityName}"
                              datasourceClass="com.company.rgr.web.customdatasources.${entityName}Datasource"/>
    </dsContext>
    ...
    <layout>
        ...
        <dataGrid id="dg${entityName}"
                  datasource="${camelCase(entityName)}Ds">
            <columns>
                ${displayedColumns.map(column => `<column id="${column.name}"
                        caption="${column.title}"
                        property="${column.name}"/>
                `).join('')}
            </columns>
        </dataGrid>
    </layout>
</window>`;
}
