/* eslint-disable */
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const publicFolder = path.resolve(__dirname, 'dist');
const srcFolder = path.resolve(__dirname, 'src');

const plugins = process.env.NODE_ENV === 'production' ? [
  new webpack.optimize.UglifyJsPlugin({
    minimize: true,
    beautify: false,
    unused: true,
    drop_debugger: true,
    drop_console: true,
    mangle: {
      screw_ie8: true,
      keep_fnames: true,
    },
    compress: {
      screw_ie8: true,
      warnings: false,
    },
    comments: false,
    sourceMap: false
  })
] : [];

module.exports = {
  entry: [
    'babel-polyfill',
    'whatwg-fetch',
    // './src/assets/css/bootstrap.css',
    './src/app.jsx',
    './src/style.scss',
    // './src/assets/index.js',
    // 'react-select/dist/react-select.css',
    // 'react-virtualized/styles.css',
    // 'react-virtualized-select/styles.css'
  ],

  output: {
    filename: process.env.NODE_ENV !== 'production' ? '[hash].bundle.js' : '[hash].bundle.min.js',
    publicPath: '/',
    path: publicFolder
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env', 'react', 'stage-0'],
              cacheDirectory: true
            }
          }
          // , {
          //   loader: 'eslint-loader'
          // }
        ]
      }, {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader'
        })
      }, {
        test: /\.scss$/,
        use: [{
          loader: 'style-loader' // creates style nodes from JS strings
        }, {
          loader: 'css-loader' // translates CSS into CommonJS
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      }, {
        test: /\.(jpg|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: 'file-loader'
      }
    ]
  },

  devServer: {
    host: 'localhost',
    port: 1234,
    inline: true,
    contentBase: publicFolder,
    historyApiFallback: true
  },

  devtool: process.env.NODE_ENV !== 'production' ? 'source-map' : false,

  resolve: {
    modules: ['node_modules', srcFolder],
    extensions: ['*', '.js', '.jsx', '.json']
  },



  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      }
    }),

    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve(srcFolder, 'index.html')
    }),

    new ExtractTextPlugin({
      filename: '[hash].styles.css',
      allChunks: true,
      disable: process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'remote'
    }),

    new FaviconsWebpackPlugin({
      logo: './src/favicon.png',
      icons: {
        android: false,
        appleIcon: false,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: false,
        opengraph: false,
        twitter: false,
        yandex: false,
        windows: false
      }
    }),

    ...plugins
  ],

};
